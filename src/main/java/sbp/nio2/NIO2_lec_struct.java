package sbp.nio2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Consumer;

public class NIO2_lec_struct {
    public static void main(String[] args) throws IOException {

//        Path path = Paths.get("data");
//        for (int i = 1; i <= 5; i ++)
//        {
//            Files.createFile(path.resolve("file" + i + ".java"));
//            path = Files.createDirectory(path.resolve("dir" + i ));
//        }

        Path src = Paths.get("data/dump.txt");
        List<String> list = Files.readAllLines(src);
        System.out.println("list> " + list);
        list.stream().forEach(s -> System.out.println(s));
    }
}
