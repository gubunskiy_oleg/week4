package sbp.nio2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NIO2_lec_next {

    public static void main(String[] args) throws IOException {

        Path path = Paths.get("data");
        System.out.println("path.exists = " + Files.exists(path));

        if (!Files.exists(path)) Files.createDirectory(path);

        if (!Files.exists(path.resolve("dump.txt"))) Files.createFile(path.resolve("dump.txt"));

//        Path fileName = Paths.get("data/dump.txt");
//        try (BufferedWriter writer = Files.newBufferedWriter(fileName)) {
//
//            writer.write("fileName: " + fileName);
//            writer.write("\n");
//            writer.write("write by Files.newBufferedWriter");
//        } catch (Exception ex) {ex.printStackTrace();}


//        System.out.println("FileContent: \n");
//        try (BufferedReader reader = Files.newBufferedReader(Paths.get("data/dump.txt"))) {
//            String currentLine  = null;
//            while ((currentLine = reader.readLine()) != null) {
//                System.out.println(currentLine);
//            }
//        }


        System.out.println(Paths.get("copy/test.txt").getParent().resolve("test_copy.txt"));
        //Files.createDirectory(Paths.get("copy"));
        Files.copy(Paths.get("data/dump.txt"),
                Paths.get("copy/test.txt").getParent().resolve("test_copy.txt"));
    }
}
