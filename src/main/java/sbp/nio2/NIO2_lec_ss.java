package sbp.nio2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class NIO2_lec_ss {
    public static void main(String[] args) throws IOException {

        Path path = Paths.get("data");
        Stream<Path> stream = Files.walk(path, 1);

        stream
                //.filter((p) -> p.toString().endsWith(".txt"))
                .forEach((p) -> System.out.println(p));
    }
}
