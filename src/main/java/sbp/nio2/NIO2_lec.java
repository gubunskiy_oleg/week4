package sbp.nio2;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NIO2_lec {

    public static void main(String[] args) {

        Path path = Paths.get("C:/test/hello.java");
        System.out.println("path> " + path);
        System.out.println("isAbsolute> " + path.isAbsolute());
        System.out.println("getFileName> " + path.getFileName());

        System.out.println("***********************************");

        path = Paths.get("test.txt");
        System.out.println("isDirectory> " + Files.isDirectory(path));
        System.out.println("isRegularFile> " + Files.isRegularFile(path));
        System.out.println("exists> " + Files.exists(path));

        Path path1 = Paths.get("C:/test/hello.java");
        Path path2 = Paths.get("C:/test/work/");

        Path relativePath =  path2.relativize(path1);
        System.out.println("path2.relativize(path1) = " + relativePath);

        Path pathToHelloJava = path2.resolve(relativePath);
        System.out.println("pathToHelloJava = " + pathToHelloJava);

        Path normalizedPath = pathToHelloJava.normalize();
        System.out.println("normalizedPath = " + normalizedPath);

        String root = path1.getRoot().toString();
        System.out.println("root = " + root);

        Path subpath = path1.subpath(0, 2);
        System.out.println("subpath = " + subpath);

        Path parent = path1.getParent();
        System.out.println("parent = " + parent);
    }
}
