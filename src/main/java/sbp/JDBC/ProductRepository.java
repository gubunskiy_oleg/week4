package sbp.JDBC;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ProductRepository {

    private final Connection connection;

    public ProductRepository(String jdbc_url) throws SQLException {
        this.connection = DriverManager
                .getConnection(jdbc_url);
    }

    public Connection getConnection() {
        return connection;
    }

    public void createTable() throws SQLException {

        Statement stmt = connection.createStatement();
        String sql = "create table person(" +
                "id varchar(32)," +
                "name varchar(32)," +
                "date varchar(32)" +
                ");";

        int affectedRows = stmt.executeUpdate(sql);
        System.out.println("createTable completed affectedRows> " + affectedRows);
    }

    public void dropTable() throws SQLException {

        Statement stmt = connection.createStatement();
        String sql = "DROP TABLE person";
        int affectedRows = stmt.executeUpdate(sql);
        System.out.println("dropTable completed affectedRows> " + affectedRows);
    }

    public void addColumn(String id, String name, String date) throws SQLException {

        Statement stmt = connection.createStatement();
        String sql = "insert into person " +
                "('id', 'name', 'date')" +
                "values" +
                "(" + id + ", " + name + ", " + date + ")";
        int affectedRows = stmt.executeUpdate(sql);
        System.out.println("dropTable completed affectedRows> " + affectedRows);
    }
        public void addPerson(Person prs) throws SQLException {

        Statement stmt = connection.createStatement();
        String sql = String.format("insert into person " +
                "('id', 'name', 'date')" +
                "values" +
                "('%s', '%s', '%s')", prs.getId(), prs.getName(), prs.getDate().toString());

        boolean hasResultSet = stmt.execute(sql);
        int affectedRows = stmt.getUpdateCount();
        System.out.println("dropTable completed hasResultSet> " + hasResultSet + " affectedRows> " + affectedRows);
    }

    public void changeName(String oldName, String newName) throws SQLException {

        Statement stmt = connection.createStatement();
        String sql = String.format("update person set name = '%s' WHERE name = '%s'", newName, oldName);
        boolean hasResultSet = stmt.execute(sql);
        int affectedRows = stmt.getUpdateCount();
        System.out.println("changeName completed hasResultSet> " + hasResultSet + " affectedRows> " + affectedRows);
    }

    public List<Person> findAll() throws SQLException {

        Statement stmt = connection.createStatement();
        String sql = String.format("select * from person");

        List<Person> people = new ArrayList<>();
        ResultSet resultSet = stmt.executeQuery(sql);
        while (resultSet.next()){

            String id = resultSet.getString("id");
            String name = resultSet.getString("name");
            LocalDate date = LocalDate.parse(resultSet.getString("date"));
            Person person = new Person(id, name, date);
            people.add(person);
        }
        return people;
    }


    public List<Person> findByName(String nameBy) throws SQLException {


        Statement stmt = connection.createStatement();
        String sql = String.format("select * from person where name = '%s'", nameBy);

        List<Person> people = new ArrayList<>();
        ResultSet resultSet = stmt.executeQuery(sql);
        while (resultSet.next()){

            String id = resultSet.getString("id");
            String name = resultSet.getString("name");
            LocalDate date = LocalDate.parse(resultSet.getString("date"));
            Person person = new Person(id, name, date);
            people.add(person);
        }
        return people;
    }
    public int deleteById(String idBy) throws SQLException {

        String sql = String.format("delete from person where id = ?");
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, idBy);

        int affectedRows = stmt.executeUpdate();
        System.out.println("deleteById completed id> " + idBy + " affectedRows> " + affectedRows);
        return affectedRows;
    }

}
