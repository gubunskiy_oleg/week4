package sbp.collections;

import org.eclipse.rdf4j.query.algebra.In;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamAPI {

    public static void main(String[] args) {

        List list = Arrays.asList('(','[','}','[',']','(',')',')');
        Deque<Character> stack = new ArrayDeque<>();



        System.out.println(list);

        List lst = (List) list.stream()
                .filter((el)->{
                    System.out.println("el = " + el);
                    if(el.equals('(')) {
                        stack.push(')');
                    } else if (el.equals('{')) {
                        stack.push('}');
                    } else if (el.equals('[')) {
                        stack.push(']');
                    }  else if (el.equals(stack.peekFirst())) {
                        stack.pop();
                    } else {
                        return true;
                    }
                    return false;
                }).collect(Collectors.toList());

        System.out.println(lst);
        System.out.println(stack);

        Deque<Character> stk = new ArrayDeque<>();
        "([][[]())".chars()
                .filter((el)->{
                    System.out.println("([][]())" + " -> " + stk);
                    if (el == '(') {
                        stk.push(')');
                    } else if (el == '{') {
                        stk.push('}');
                    } else if (el == '[') {
                        stk.push(']');
                    }  else if (el == stk.peekFirst()) {
                        stk.pop();
                    } else {
                        return true;
                    }
                    return false;
                })
                .forEach((c)-> System.out.println(" >" + c + "< "));
    }
}
