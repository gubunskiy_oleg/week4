package sbp.collections;


import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class CustomDigitComparatorTest {

    /**
     *      We test the comparator to meet the conditions of the task
     *      first even, then odd
     */

    @Test
    public void compare() {
        List<Integer> list = new LinkedList<>();

        for (int i = 8; i > -9; i--)
            list.add(i);

        list.add(15);
        list.add(50);
        list.add(-200);
        list.add(-12);
        list.add(2);
        list.add(-1);

        list.sort(new CustomDigitComparator());

        int cnt = 0;

        for (Integer el: list)
        {
            if (el % 2 == 0) cnt++;
        }

        for (int i = 0; i < cnt; i++)
            Assert.assertTrue(list.get(i) % 2 == 0);

        for (int i = cnt; i < list.size() - 1; i++)
            Assert.assertTrue(Math.abs(list.get(i) % 2) == 1);

        Assert.assertTrue(list.get(0) % 2 == 0);
        Assert.assertTrue(Math.abs(list.get(list.size() - 1) % 2) == 1);
    }
}