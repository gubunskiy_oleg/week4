package sbp.collections;

import java.util.Date;
import java.util.Objects;

/**
 * Напишите реализацию класса User (создайте класс, добавьте поля, конструкторы,геттеры,
 * переопределите необходимые методы).
 */
public class User {
    private String name;
    private String email;
    private String phoneNumber;
    private String address;
    private String role;
    private int age;
    private boolean verified;
    private boolean active;
    private Date lastModified;
    private Date created;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public User(){};

    public User(String name, String email, String phoneNumber, String address, String role) {
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.role = role;
        this.created = new Date();
        this.lastModified = this.created;
    }

    public User(String name, String email, String phoneNumber, String role) {
        this(name, email, phoneNumber, "", role);
    }

    public User(User other) {
        this.name = other.name;
        this.email = other.email;
        this.phoneNumber = other.phoneNumber;
        this.address = other.address;
        this.role = other.role;
        this.verified = other.verified;
        this.active = other.active;
        this.created = new Date();
        this.lastModified = this.created;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public String getRole() {
        return role;
    }

    public boolean isVerified() {
        return verified;
    }

    public boolean requestByVerified(boolean verified) {
        //check phoneNumber and email
        this.verified = verified;
        return true;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", role='" + role + '\'' +
                '}' + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return verified == user.verified && active == user.active && Objects.equals(name, user.name)
                && Objects.equals(email, user.email) && Objects.equals(phoneNumber, user.phoneNumber)
                && Objects.equals(address, user.address) && Objects.equals(role, user.role)
                && Objects.equals(lastModified, user.lastModified) && Objects.equals(created, user.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email, phoneNumber, address, role, verified, active, lastModified, created);
    }
}
