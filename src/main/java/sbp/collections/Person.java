package sbp.collections;

import java.util.Comparator;
import java.util.Objects;

/**
 *    Класс Person{name, city, age}, определить метод toString.
 *    Класс Person реализует интерфейс Comparable<Person>, который обеспечивает следующий порядок:
 *    - Сортировка сначала по полю city, а затем по полю name;
 *    - Поля name, city отличны от null;
 */

public class Person implements Comparable<Person>{

    private String name;
    private String city;
    private Integer age;

    public Person(String name, String city, Integer age) {
        if (Objects.nonNull(name) && Objects.nonNull(city)) {

            this.name = name;
            this.city = city;
            this.age = age;
        } else {
            throw new RuntimeException("Object person do not be create with 'null' field");
        }
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                "}\n";
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * @param other the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     *
     * if fields "city" or "name" are null, sort puts the record at the end
     */
    public int compareTo(Person other) {

        int result = this.getCity().compareToIgnoreCase(other.getCity());

        if (result == 0)
        {
            return this.getName().compareToIgnoreCase(other.getName());
        } else {
            return result;
        }
    }
}
