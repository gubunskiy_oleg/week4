package sbp.collections;

import java.util.*;

public class MapEntry {

    public static void main(String[] args) {

        Map<String, String> map = new HashMap<>();

        map.put("one","one");
        map.put("tow","tow");
        map.put("three","three");
        map.put("four","four");
        map.put("five","five");
        map.put("six","six");
        map.put("seven","seven");
        map.put("eight","eight");
        map.put("nine","nine");
        map.put("ten","ten");

        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while(iterator.hasNext())
        {
            Map.Entry<String, String> mp = iterator.next();
            System.out.println(mp.getKey() + " \t " + mp.getValue() + " \t - > " + mp);
        }


    }
}
