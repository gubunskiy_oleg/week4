package sbp.collections;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Serializations {

    public static void main(String[] args) throws JsonProcessingException {

        User user = new User();
        user.setName("Ivan");
        user.setAge(20);
        user.setEmail("ivi@mail.com");
        user.setPhoneNumber("8-923-659-32-98");
        user.setRole("admin");

        XmlMapper xmlMapper = new XmlMapper();
        String xmlUser = xmlMapper.writeValueAsString(user);
        System.out.println("XML user value> " + xmlUser);

        User userFromXml = xmlMapper.readValue(xmlUser, User.class);
        System.out.println("User from xml> " + userFromXml);

        Gson gson = new GsonBuilder().create();
        String jsonUser = gson.toJson(user);
        System.out.println("Json user value> " + jsonUser);
        User userFromJson = gson.fromJson(jsonUser, User.class);
        System.out.println("User from json> " + userFromJson);
    }
}
