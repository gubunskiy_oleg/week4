package sbp.collections;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.commons.lang.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

public class FindDuplicates {

     //Let's train on cats

    /**
     * Method checks a sequence of characters in brackets for correctness
     *
     * @param s input string opened and closed parentheses
     * @return true if the sequence is correct, false if incorrect
     */
    public static boolean parentheses(String s) {

        if (s.length() % 2 != 0) return false;
        char[] chars = s.toCharArray();
        char[] stack = new char[chars.length];
        int j = 0;
        for (int i = 0; i < chars.length; i++)
        {

            if (chars[i] == '(') {

                stack[j++] = ')';
            }
            else if (chars[i] == '{') {

                stack[j++] = '}';
            }
            else if (chars[i] == '[') {

                stack[j++] = ']';
            }
            else if (chars[i] == stack[j - 1])
            {
                --j;
            }
            else {
                throw new RuntimeException("incorrect expression on step> "
                        + i + " stack position> " + j);
            }
        }

        if (j == 0){
            return true;
        } else {
            return false;}
    }

    /**
     * When a new element is added to the set, false is returned
     * when the element is already in the collection
     * stream filter only passes on when elements are duplicated
     * then a new set is formed
     * the equals method must be overridden in the collection element class
     *
     * @param collection input collection of elements any type
     * @param <T> type of collection
     * @return collection duplicates of elements
     */
    public static <T> Collection<T> findThirdMethod(Collection<T> collection)
    {

        Set items = new HashSet<>();
        return collection.stream()
                .filter(n -> !items.add(n))
                .collect(Collectors.toSet());
    }

    /**
     * Double loop with iterators
     * when the elements match, we write to the collection of duplicates
     * the equals method must be overridden in the collection element class
     *
     * @param collection input collection of elements any type
     * @param <T> type of collection
     * @return collection duplicates of elements
     */
    public static <T> Collection<T> findSecondMethod(Collection<T> collection)
    {

        List<T> duplicates = new LinkedList<>();
        List<T> collectionList = new LinkedList<T>(collection);
        ListIterator it = collectionList.listIterator();
        ListIterator it2;

        while (it.hasNext())
        {
            T lhs = (T) it.next();
            if (it.hasNext())
            {
                it2 = collectionList.listIterator(it.nextIndex());
            } else {
                return duplicates;
            }
            while (it2.hasNext()) {
                T rhs = (T) it2.next();
                if (lhs.equals(rhs) && !duplicates.contains(rhs)) {
                    duplicates.add(lhs);
                }
            }
        }
        return duplicates;
    }

    /**
     * Pass collection to set,
     * CROSS JOIN for collection and set, will give duplicates
     * the equals method must be overridden in the collection element class
     *
     * @param collection input collection of elements any type
     * @param <T> type of collection
     * @return collection duplicates of elements
     */
    public static <T> Collection<T> findFirstMethod(Collection<T> collection)
    {

        List<T> duplicates = new ArrayList<>(collection);
        Set<T> set = new LinkedHashSet<>(collection);
        for (T el: set)
        {
            duplicates.remove(el);
        }
        Set<T> duplicatesSet = new LinkedHashSet<>(duplicates);
        return duplicatesSet;
    }
}
