package sbp.collections;

import java.util.Comparator;
    /**
     *      Компаратор CustomDigitComparator, который реализует интерфейс Comparator<Integer>.
     *      Класс CustomDigitComparator определяет следующий порядок:
     *       - Сначала четные числа, затем нечетные;
     *       - На вход подаются числа, отличные от null;
     */
public class CustomDigitComparator implements Comparator<Integer>{

    /**
     * Compares its two arguments for order.  Returns a negative integer,
     * zero, or a positive integer as the first argument is less than, equal
     * to, or greater than the second.
     *
     *      first even, then odd
     *      everything is in ascending order
     *
     * @param lhs the first object to be compared.
     * @param rhs the second object to be compared.
     * @return a negative integer, zero, or a positive integer as the
     * first argument is less than, equal to, or greater than the
     * second.
     */

    @Override
    public int compare(Integer lhs, Integer rhs) {
        if (lhs == rhs)
        {
            return 0;
        }
        if (lhs % 2 == 0)
        {
            if (rhs % 2 == 0)
            {
                if (lhs < rhs)
                {
                    return -1;
                } else {
                    return 1;
                }
            } else {
                return -1;
            }
        } else {
            if (rhs % 2 == 0)
            {
                return 1;
            } else {
                if (lhs < rhs)
                {
                    return -1;
                } else {
                    return 1;
                }
            }
        }
    };
}
