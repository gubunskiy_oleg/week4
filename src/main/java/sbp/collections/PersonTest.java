package sbp.collections;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
//import org.mockito.Mockito;

import java.util.LinkedList;
import java.util.List;

public class PersonTest {

    /**
     *      Checking the output string for type matching String.class
     */
    @Test
    public void testToString() {

        List<Person> list = new LinkedList<>();
        list.add(new Person("Mihail", "Omsk", 35));

        Assert.assertTrue(list.toString().getClass().equals(String.class));
    }

    @Test
    /**
     *      Checking sorting in ascending order on the "city" field,
     *      when "city" without changes, check sorting by "name" field,
     *      check throwing exception if 'city' or 'name' fields are null
     *
     *      @Code {el.getCity().compareToIgnoreCase(city) >= 0}, when
     *      the next element is greater than or equal lexicographically
     */
    public void compareTo() {

        List<Person> list = new LinkedList<>();

        list.add(new Person("Mihail", "Omsk", 35));
        list.add(new Person("Fedor", "Piter", 33));
        list.add(new Person("Petr", "omsk", 23));
        list.add(new Person("Denis", "moscow", 28));
        list.add(new Person("Anton", "Moscow", 25));
        list.add(new Person("Alex", "Omsk", 26));
        list.add(new Person("null", "Omsk", 26));

        list.sort(Person::compareTo);

        String city = list.get(0).getCity();
        String name = list.get(0).getName();
        for (Person el: list) {

            Assert.assertTrue(el.getCity().compareToIgnoreCase(city) >= 0);
            if (el.getCity().compareToIgnoreCase(city) == 0) {

                Assert.assertTrue(el.getName().compareToIgnoreCase(name) >= 0);
            }
            city = el.getCity();
            name = el.getName();
        }

        Assertions.assertThrows(RuntimeException.class,() -> new Person(null, "Omsk", 26));
    }
}