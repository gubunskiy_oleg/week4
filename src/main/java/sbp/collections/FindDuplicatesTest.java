package sbp.collections;


import org.junit.jupiter.api.Assertions;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

public class FindDuplicatesTest {

    /**
     *  Checking: all duplicate elements are included in the input collection more than once
     *  By types: List<String> Queue<Integer> Deque<User>
     */
    @Test
    public void findThirdMethod() {

        List<String> list = new ArrayList<>();
        list.add("(^._.^)"); list.add("(^･ｪ･^)"); list.add("(=^･ω･^=)"); list.add("(˃ᆺ˂)");
        list.add("(^・x・^)"); list.add("(=‐ω‐=)"); list.add("(˃ᆺ˂)"); list.add("ฅ•ω•ฅ");
        list.add("(ΦωΦ)"); list.add("(^･ｪ･^)"); list.add("ミ๏ｖ๏彡"); list.add("(˃ᆺ˂)");
        list.add("(=‘ｘ‘=)"); list.add("(^._.^)");

        Assert.assertTrue(FindDuplicates.findSecondMethod(list).size() > 0);
        Collection<String> duplicatesStr = FindDuplicates.findSecondMethod(list);
        Integer sizeList = list.size();
        Assert.assertTrue(
                duplicatesStr.stream()
                        .filter((el) -> list.remove(el))
                        .filter((el) -> list.remove(el))
                        .collect(Collectors.toList())
                        .equals(duplicatesStr));
        Assert.assertTrue(sizeList > list.size() + duplicatesStr.size() );

        Queue<Integer> queue = new ArrayDeque<>();
        queue.add(123); queue.add(321); queue.add(333); queue.add(222);
        queue.add(123); queue.add(333); queue.add(321);

        Assert.assertTrue(FindDuplicates.findSecondMethod(queue).size() > 0);
        Collection<Integer> duplicatesInt = FindDuplicates.findSecondMethod(queue);
        Integer sizeQueue = queue.size();
        Assert.assertTrue(
                duplicatesInt.stream()
                        .filter((el) -> queue.remove(el))
                        .filter((el) -> queue.remove(el))
                        .collect(Collectors.toList())
                        .equals(duplicatesInt));
        Assert.assertTrue(sizeList > queue.size() + duplicatesInt.size() );


        Deque<User> deque = new ArrayDeque<>();
        deque.add(new User("Anton", "asq@mail.ru", "8-913-455-25-89", "user"));
        deque.add(new User("Dimon", "its@mail.ru", "8-932-425-22-39", "adm"));
        deque.add(new User("Anton", "asq@mail.ru", "8-913-455-25-89", "user"));

        Assert.assertTrue(FindDuplicates.findSecondMethod(deque).size() > 0);
        Collection<User> duplicatesUsr = FindDuplicates.findSecondMethod(deque);
        Integer sizeDeque = deque.size();
        Assert.assertTrue(
                duplicatesUsr.stream()
                        .filter((el) -> deque.remove(el))
                        .filter((el) -> deque.remove(el))
                        .collect(Collectors.toList())
                        .equals(duplicatesUsr));
        Assert.assertTrue(sizeList > deque.size() + duplicatesUsr.size() );
    }

    /**
     *  Checking: all duplicate elements are included in the input collection more than once
     *  By types: List<String> Queue<Integer> Deque<User>
     */
    @Test
    public void findSecondMethod() {

        List<String> list = new ArrayList<>();
        list.add("(^._.^)"); list.add("(^･ｪ･^)"); list.add("(=^･ω･^=)"); list.add("(˃ᆺ˂)");
        list.add("(^・x・^)"); list.add("(=‐ω‐=)"); list.add("(˃ᆺ˂)"); list.add("ฅ•ω•ฅ");
        list.add("(ΦωΦ)"); list.add("(^･ｪ･^)"); list.add("ミ๏ｖ๏彡"); list.add("(˃ᆺ˂)");
        list.add("(=‘ｘ‘=)"); list.add("(^._.^)");

        Assert.assertTrue(FindDuplicates.findSecondMethod(list).size() > 0);
        Collection<String> duplicatesStr = FindDuplicates.findSecondMethod(list);
        Integer sizeList = list.size();
        Assert.assertTrue(
                duplicatesStr.stream()
                        .filter((el) -> list.remove(el))
                        .filter((el) -> list.remove(el))
                        .collect(Collectors.toList())
                        .equals(duplicatesStr));
        Assert.assertTrue(sizeList > list.size() + duplicatesStr.size() );

        Queue<Integer> queue = new ArrayDeque<>();
        queue.add(123); queue.add(321); queue.add(333); queue.add(222);
        queue.add(123); queue.add(333); queue.add(321);

        Assert.assertTrue(FindDuplicates.findSecondMethod(queue).size() > 0);
        Collection<Integer> duplicatesInt = FindDuplicates.findSecondMethod(queue);
        Integer sizeQueue = queue.size();
        Assert.assertTrue(
                duplicatesInt.stream()
                        .filter((el) -> queue.remove(el))
                        .filter((el) -> queue.remove(el))
                        .collect(Collectors.toList())
                        .equals(duplicatesInt));
        Assert.assertTrue(sizeList > queue.size() + duplicatesInt.size() );


        Deque<User> deque = new ArrayDeque<>();
        deque.add(new User("Anton", "asq@mail.ru", "8-913-455-25-89", "user"));
        deque.add(new User("Dimon", "its@mail.ru", "8-932-425-22-39", "adm"));
        deque.add(new User("Anton", "asq@mail.ru", "8-913-455-25-89", "user"));

        Assert.assertTrue(FindDuplicates.findSecondMethod(deque).size() > 0);
        Collection<User> duplicatesUsr = FindDuplicates.findSecondMethod(deque);
        Integer sizeDeque = deque.size();
        Assert.assertTrue(
                duplicatesUsr.stream()
                        .filter((el) -> deque.remove(el))
                        .filter((el) -> deque.remove(el))
                        .collect(Collectors.toList())
                        .equals(duplicatesUsr));
        Assert.assertTrue(sizeList > deque.size() + duplicatesUsr.size() );
    }

    /**
     *  Checking: all duplicate elements are included in the input collection more than once
     *  By types: List<String> Queue<Integer> Deque<User>
     */
    @Test
    public void findFirstMethod() {

        List<String> list = new ArrayList<>();
        list.add("(^._.^)"); list.add("(^･ｪ･^)"); list.add("(=^･ω･^=)"); list.add("(˃ᆺ˂)");
        list.add("(^・x・^)"); list.add("(=‐ω‐=)"); list.add("(˃ᆺ˂)"); list.add("ฅ•ω•ฅ");
        list.add("(ΦωΦ)"); list.add("(^･ｪ･^)"); list.add("ミ๏ｖ๏彡"); list.add("(˃ᆺ˂)");
        list.add("(=‘ｘ‘=)"); list.add("(^._.^)");

        Assert.assertTrue(FindDuplicates.findSecondMethod(list).size() > 0);
        Collection<String> duplicatesStr = FindDuplicates.findSecondMethod(list);
        Integer sizeList = list.size();
        Assert.assertTrue(
                duplicatesStr.stream()
                        .filter((el) -> list.remove(el))
                        .filter((el) -> list.remove(el))
                        .collect(Collectors.toList())
                        .equals(duplicatesStr));
        Assert.assertTrue(sizeList > list.size() + duplicatesStr.size() );

        Queue<Integer> queue = new ArrayDeque<>();
        queue.add(123); queue.add(321); queue.add(333); queue.add(222);
        queue.add(123); queue.add(333); queue.add(321);

        Assert.assertTrue(FindDuplicates.findSecondMethod(queue).size() > 0);
        Collection<Integer> duplicatesInt = FindDuplicates.findSecondMethod(queue);
        Integer sizeQueue = queue.size();
        Assert.assertTrue(
                duplicatesInt.stream()
                        .filter((el) -> queue.remove(el))
                        .filter((el) -> queue.remove(el))
                        .collect(Collectors.toList())
                        .equals(duplicatesInt));
        Assert.assertTrue(sizeList > queue.size() + duplicatesInt.size() );


        Deque<User> deque = new ArrayDeque<>();
        deque.add(new User("Anton", "asq@mail.ru", "8-913-455-25-89", "user"));
        deque.add(new User("Dimon", "its@mail.ru", "8-932-425-22-39", "adm"));
        deque.add(new User("Anton", "asq@mail.ru", "8-913-455-25-89", "user"));

        Assert.assertTrue(FindDuplicates.findSecondMethod(deque).size() > 0);
        Collection<User> duplicatesUsr = FindDuplicates.findSecondMethod(deque);
        Integer sizeDeque = deque.size();
        Assert.assertTrue(
                duplicatesUsr.stream()
                        .filter((el) -> deque.remove(el))
                        .filter((el) -> deque.remove(el))
                        .collect(Collectors.toList())
                        .equals(duplicatesUsr));
        Assert.assertTrue(sizeList > deque.size() + duplicatesUsr.size() );
    }

    /**
     *  Testing true sequence,
     *  testing false sequence if length of string odd,
     *  testing true sequence,
     *  testing throw exception if the sequence is confused
     */
    @Test
    public void parentheses()
    {
        Assert.assertTrue(FindDuplicates.parentheses("(({(){}{}[]()}))"));
        Assert.assertFalse(FindDuplicates.parentheses("(({(){}{}[())]()}))"));
        Assert.assertTrue(FindDuplicates.parentheses("(({(){(())}[(){()}]()})(){}[]())"));
        Assertions.assertThrows(RuntimeException.class,
                () -> FindDuplicates.parentheses("([]({[[[{(()()]()]{}]}))"));
    }

}